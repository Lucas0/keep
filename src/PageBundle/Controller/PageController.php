<?php

namespace PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Manages the main pages of the website.
 */
class PageController extends Controller
{
    /**
     * Homepage of the website.
     *
     * @return Response A Response instance
     */
    public function homeAction()
    {
        $manager = $this->getDoctrine()->getManager();

        $projects = $manager->getRepository('ProjectBundle:Project')->getLastProjects();
        $campuses = $manager->getRepository('ProjectBundle:Campus')->findAll();
        $categories = $manager->getRepository('ProjectBundle:Category')->findBy([], ['name' => 'ASC']);

        return $this->render('PageBundle:Page:home.html.twig', compact('projects', 'categories', 'campuses'));
    }

    /**
     * About page of the website.
     *
     * @return Response A Response instance
     */
    public function aboutAction()
    {
        return $this->render('PageBundle:Page:about.html.twig');
    }

    /**
     * Contact page of the website.
     *
     * @return Response A Response instance
     */
    public function contactAction()
    {
        return $this->render('PageBundle:Page:contact.html.twig');
    }

    /**
     * Page where users are asked to wait until the container is loaded.
     *
     * @return Response A Response instance
     */
    public function containerLoadingAction()
    {
        return $this->render('PageBundle:Page:containerLoading.html.twig');
    }
}
