<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * User controller.
 */
class UserController extends Controller
{
    /**
     * Json API for user searching.
     *
     * @param string $keyword The search keyword
     *
     * @return JsonResponse A JsonResponse instance
     *
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function searchAction(string $keyword)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('UserBundle:User');
        $uid_key = $this->getParameter('app.ldap_uid_key');

        // Adding new users to the database
        $ldapUserProvider = $this->get('custom_ldap_user_provider');
        $search = $ldapUserProvider->searchUser($keyword, true);

        foreach ($search as $res) {
            $username = $res->getAttribute($uid_key)[0];
            $user = $ldapUserProvider->loadUser($username, $res, false);
        }

        // Retrieving users from the database
        $query = $repo->createQueryBuilder('u')->select('u.id, u.username, u.firstname, u.lastname');
        $query->andWhere('u.username LIKE :name');
        $res = $query->setParameter('name', '%'  . $keyword . '%')
            ->getQuery()
            ->getArrayResult();

        return new JsonResponse($res);
    }
}
