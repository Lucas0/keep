<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller managing user security.
 */
class SecurityController extends Controller
{
    /**
     * Manages the user login.
     */
    public function loginAction(Request $request)
    {
        $authorizationChecker = $this->get('security.authorization_checker');

        $routes = [
            'ROLE_ADMIN' => 'admin_home',
            'ROLE_USER' => 'project_account_index'
        ];

        foreach ($routes as $role => $route) {
            // If the user is already logged, we can redirect
            if ($authorizationChecker->isGranted($role)) {
                // if the user has been redirected to the login page
                if ($this->get('session')->has('loginReferer')) {
                    $referer = $this->get('session')->remove('loginReferer');
                    return $this->redirect('//' . $referer);
                }

                return $this->redirectToRoute($route);
            }
        }

        $authenticationUtils = $this->get('security.authentication_utils');

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('UserBundle:Security:login.html.twig', array(
            'last_username' => $authenticationUtils->getLastUsername(),
            'error'         => $authenticationUtils->getLastAuthenticationError(),
            'csrf_token' => $csrfToken,
        ));
    }
}
