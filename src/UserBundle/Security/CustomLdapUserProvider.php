<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UserBundle\Security;

use Symfony\Component\Ldap\Entry;
use Symfony\Component\Security\Core\Exception\InvalidArgumentException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Ldap\Exception\ConnectionException;
use Symfony\Component\Ldap\LdapInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;
use UserBundle\Entity\User;

/**
 * LdapUserProvider is a simple user provider on top of ldap.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 * @author Charles Sarrazin <charles@sarraz.in>
 */
class CustomLdapUserProvider implements UserProviderInterface
{
    private $ldap;
    private $baseDn;
    private $searchDn;
    private $searchPassword;
    private $defaultRoles;
    private $uidKey;
    private $defaultSearch;
    private $em;
    private $session;
    private $bound;

    /**
     * @param LdapInterface $ldap
     * @param string        $baseDn
     * @param string        $searchDn
     * @param string        $searchPassword
     * @param array         $defaultRoles
     * @param string        $uidKey
     * @param string        $filter
     * @param string        $passwordAttribute
     */
    public function __construct(LdapInterface $ldap, $baseDn, $searchDn = null, $searchPassword = null, array $defaultRoles = array(), $uidKey = 'sAMAccountName', $filter = '({uid_key}={username})')
    {
        if (null === $uidKey) {
            $uidKey = 'sAMAccountName';
        }

        $this->ldap = $ldap;
        $this->baseDn = $baseDn;
        $this->searchDn = $searchDn;
        $this->searchPassword = $searchPassword;
        $this->defaultRoles = $defaultRoles;
        $this->uidKey = $uidKey;
        $this->defaultSearch = str_replace('{uid_key}', $uidKey, $filter);
        $this->bound = false;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($credentials)
    {
        $username = $credentials['username'];
        $password = $credentials['password'];

        try {
            $entries = $this->searchUser($username);
        } catch (ConnectionException $e) {
            throw new BadCredentialsException(sprintf('User "%s" not found.', $username), 0, $e);
        }

        $count = count($entries);

        if ($count < 1) {
            throw new BadCredentialsException(sprintf('User "%s" not found.', $username));
        }

        $entry = $entries[0];

        // Trying to bind with the user credentials
        $sess = $this->ldap;
        try {
            $sess->bind($entry->getDn(), $password);
        } catch (ConnectionException $e) {
            throw new BadCredentialsException(sprintf('User "%s" not found.', $username), 0, $e);
        }

        try {
            if (null !== $this->uidKey) {
                $username = $this->getAttributeValue($entry, $this->uidKey);
            }
        } catch (InvalidArgumentException $e) {
        }

        $user = $this->loadUser($username, $entry);

        if($user->isEnabled() !== true) {
            throw new DisabledException();
        }

	return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->session->get('user');
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return $class === 'UserBundle\Entity\User';
    }

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    public function setSession(Session $session)
    {
        $this->session = $session;
    }

    /**
     * Retrieves user list from ldap server.
     * 
     * @param string $username
     * @param bool   $wildcarded Appends a wildcard a the end of the username
     * 
     * @return CollectionInterface|Entry[]
     */
    public function searchUser(string $username, $wildcarded = false) {
        $this->bind();

        $username = $this->ldap->escape($username, '', LdapInterface::ESCAPE_FILTER);
        if($wildcarded === true) {
            $username .= '*';
        }
        $query = str_replace('{username}', $username, $this->defaultSearch);
        $req = $this->ldap->query($this->baseDn, $query);
        return $req->execute();
    }

    /**
     * Loads a user from an LDAP entry.
     *
     * @param string $username
     * @param Entry  $entry
     * @param bool  $persist
     *
     * @return User
     */
    public function loadUser($username, Entry $entry, bool $persist = true)
    {
        $user = new User($username, $this->defaultRoles);
        $user = $this->getCustomUser($user, $entry);

        $userRepo = $this->em->getRepository('UserBundle:User');
        $res = $userRepo->findOneByUsername($username);

        if ($res === null) {
            $this->em->persist($user);
            $this->em->flush($user);
            $res = $user;
        } elseif ($res->isAdmin()) {
            $user->setAdmin(true);
        }

        if($persist === true) {
            $this->session->set('user', $res);
        }

        $user->setEnabled($res->isEnabled());

        return $user;
    }

    private function getCustomUser(User $user, Entry $entry) : User
    {
        if ($entry->hasAttribute('sn')) {
            $user->setLastname($entry->getAttribute('sn')[0]);
        }

        if ($entry->hasAttribute('givenName')) {
            $user->setFirstname($entry->getAttribute('givenName')[0]);
        }

        return $user;
    }

    /**
     * Fetches a required unique attribute value from an LDAP entry.
     *
     * @param null|Entry $entry
     * @param string     $attribute
     */
    private function getAttributeValue(Entry $entry, $attribute)
    {
        if (!$entry->hasAttribute($attribute)) {
            throw new InvalidArgumentException(sprintf('Missing attribute "%s" for user "%s".', $attribute, $entry->getDn()));
        }

        $values = $entry->getAttribute($attribute);

        if (1 !== count($values)) {
            throw new InvalidArgumentException(sprintf('Attribute "%s" has multiple values.', $attribute));
        }

        return $values[0];
    }

    /**
     * Binds with the default LDAP user if not bound.
     */
    private function bind() {
        if($this->bound === false) {
            $this->ldap->bind($this->searchDn, $this->searchPassword);
        }

        $this->bound = true;
    }
}
