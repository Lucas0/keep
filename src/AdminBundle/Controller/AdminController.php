<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Administration main pages.
 */
class AdminController extends Controller
{
    /**
     * Administration homepage.
     *
     * @return Response A Response instance
     */
    public function indexAction()
    {
        $manager = $this->getDoctrine()->getManager();
        $categories = $manager->getRepository('ProjectBundle:Category')->getCategories();
        $campuses = $manager->getRepository('ProjectBundle:Campus')->getCampuses();

        return $this->render(
            'AdminBundle:Admin:index.html.twig',
            compact('categories', 'campuses')
        );
    }

    /**
     * Returns some server statistics.
     *
     * @return JsonResponse A JsonResponse instance
     */
    public function serverInfoAction()
    {
        $data = [
            'mem' => AdminController::getServerMemoryUsage(),
            'cpu' => AdminController::getServerCpuUsage()
        ];

        return new JsonResponse($data);
    }

    /**
     * Returns server memory usage in percent.
     *
     * @return float memory usage
     */
    private static function getServerMemoryUsage()
    {
        $free = shell_exec('free');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $memory_usage = $mem[2]/$mem[1]*100;
     
        return $memory_usage;
    }

    /**
     * Returns server CPU usage based on the load average.
     *
     * @return float cpu usage
     */
    private static function getServerCpuUsage()
    {
        $load = sys_getloadavg();
        return $load[0];
    }
}
