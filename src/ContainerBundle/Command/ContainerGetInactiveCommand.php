<?php

namespace ContainerBundle\Command;

use Sensio\Bundle\GeneratorBundle\Command\GeneratorCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Returns containers that are considered inactive.
 */
class ContainerGetInactiveCommand extends GeneratorCommand
{
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        // Retrieving the container
        $container = $this->getContainer();
        
        // Initializing global attributes
        $this->em = $container->get('doctrine.orm.entity_manager');
    }

    protected function configure()
    {
        $this
        ->setName('container:get-inactive')
        ->setDescription('Return inactive containers');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repo = $this->em->getRepository('ContainerBundle:Container');
        $containers = $repo->getInactiveContainers();
        foreach ($containers as $container) {
            $output->writeln($container->getId());
        }
    }

    /**
     * {@inheritDoc}
     */
    protected function createGenerator()
    {
    }
}
