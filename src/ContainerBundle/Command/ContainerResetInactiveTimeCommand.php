<?php

namespace ContainerBundle\Command;

use Sensio\Bundle\GeneratorBundle\Command\GeneratorCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Put a container in inactive mode.
 */
class ContainerResetInactiveTimeCommand extends GeneratorCommand
{
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        // Retrieving the container
        $container = $this->getContainer();
        
        // Initializing global attributes
        $this->em = $container->get('doctrine.orm.entity_manager');
    }

    protected function configure()
    {
        $this
        ->setName('container:reset-inactive')
        ->setDescription('Reset inactive time of a container')
        ->addArgument('containerId', InputArgument::REQUIRED, 'The id of the container to reset');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repo = $this->em->getRepository('ContainerBundle:Container');
        $container = $repo->find($input->getArgument('containerId'));

        if ($container === null) {
            $output->writeln('<error>This container does not exist.</error>');
        } else {
            $container->setRunning(false);

            $this->em->persist($container);
            $this->em->flush($container);
        }
    }

    /**
     * {@inheritDoc}
     */
    protected function createGenerator()
    {
    }
}
