<?php

namespace ContainerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ContainerBundle\Entity\ContainerModel;

/**
 * @ORM\Table(name="container")
 * @ORM\Entity(repositoryClass="ContainerBundle\Repository\ContainerRepository")
 */
class Container
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=15, nullable=true, unique=true)
     */
    private $ip;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_access", type="datetime", nullable=true)
     */
    private $lastAccess = null;

    /**
     * @var \Boolean
     *
     * @ORM\Column(name="running", type="boolean", nullable=false)
     */
    private $running = true;

    /**
     * @var ContainerModel
     *
     * @ORM\ManyToOne(targetEntity="ContainerModel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="container_model_id", referencedColumnName="id")
     * })
     */
    private $model;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Container
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set lastAccess
     *
     * @param \DateTime $lastAccess
     *
     * @return Container
     */
    public function setLastAccess($lastAccess)
    {
        $this->lastAccess = $lastAccess;

        return $this;
    }

    /**
     * Get lastAccess
     *
     * @return \DateTime
     */
    public function getLastAccess()
    {
        return $this->lastAccess;
    }

    /**
     * Set model
     *
     * @param ContainerModel $model
     *
     * @return Container
     */
    public function setModel(ContainerModel $model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return ContainerModel
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set running
     *
     * @param boolean $running
     *
     * @return Container
     */
    public function setRunning(bool $running)
    {
        $this->running = $running;

        return $this;
    }

    /**
     * Get running
     *
     * @return boolean
     */
    public function isRunning()
    {
        return $this->running;
    }
}
