<?php

namespace ContainerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContainerModel
 *
 * @ORM\Table(name="container_model")
 * @ORM\Entity(repositoryClass="ContainerBundle\Repository\ContainerModelRepository")
 */
class ContainerModel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=40, unique=true)
     */
    private $name;

    /**
     * @var Module[]
     *
     * @ORM\ManyToMany(targetEntity="Module")
     */
    private $modules;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ContainerModel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->modules = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add module
     *
     * @param \ContainerBundle\Entity\Module $module
     *
     * @return ContainerModel
     */
    public function addModule(\ContainerBundle\Entity\Module $module)
    {
        $this->modules[] = $module;

        return $this;
    }

    /**
     * Remove module
     *
     * @param \ContainerBundle\Entity\Module $module
     */
    public function removeModule(\ContainerBundle\Entity\Module $module)
    {
        $this->modules->removeElement($module);
    }

    /**
     * Get modules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * Check if the model has a specific module
     *
     * @param string $slug
     *
     * @return Module
     */
    public function hasModule($slug)
    {
        foreach ($this->modules as $module) {
            if (strtolower($module->getSlug()) === strtolower($slug)) {
                return $module;
            }
        }

        return null;
    }
}
