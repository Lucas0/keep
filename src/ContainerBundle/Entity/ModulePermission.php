<?php

namespace ContainerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModulePermission
 *
 * @ORM\Table(name="module_permission")
 * @ORM\Entity(repositoryClass="ContainerBundle\Repository\ModulePermissionRepository")
 */
class ModulePermission
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="rule", type="string", length=255)
     */
    private $rule;

    /**
     * @var bool
     *
     * @ORM\Column(name="regex", type="boolean")
     */
    private $regex;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rule
     *
     * @param string $rule
     *
     * @return ModulePermission
     */
    public function setRule($rule)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Get rule
     *
     * @return string
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * Set regex
     *
     * @param boolean $regex
     *
     * @return ModulePermission
     */
    public function setRegex($regex)
    {
        $this->regex = $regex;

        return $this;
    }

    /**
     * Get regex
     *
     * @return bool
     */
    public function isRegex()
    {
        return $this->regex;
    }
}
