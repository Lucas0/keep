<?php

namespace ContainerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ContainerBundle\Entity\Container;
use ContainerBundle\Entity\Module;

/**
 * Routing management towards containers with grant checking.
 */
class RoutingController extends Controller
{
    /**
     * Checks if the request is allowed to access to a specific container.
     *
     * This route is called by the Nginx server when a user try to access to a container.
     * The response is based on the HTTP code that Nginx will read to decide what to do.
     * The 401 and 403 status will deny the access whereas 2xx will accept it.
     *
     * @param Request $request
     *
     * @return Response A Response instance
     */
    public function grantedCheckAction(Request $request)
    {
        $authorizationChecker = $this->get('security.authorization_checker');

        $domain = $request->server->get('HTTP_X_REAL_HOST');
        $uri = $request->server->get('HTTP_X_ORIGINAL_URI');

        // Retrieving slug and app fields from the url
        $subDomains = $this->getSubDomains($domain);

        if (count($subDomains) == 1) {
            $slug = $subDomains[0];
            $app = null;
        } elseif (count($subDomains) == 2) {
            $slug = $subDomains[1];
            $app = $subDomains[0];
        } else {
            return new Response('Forbidden', 403);
        }

        if ($this->isRouteCached($app, $slug, $uri)) {
            return new Response('OK', 200);
        }

        // Checking if the project is in the database
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('ProjectBundle:Project');
        $project = $repo->getProjectBySlug($slug);

        if ($project === null || $project->getContainer() === null) {
            return new Response('Forbidden', 403);
        }

        $container = $project->getContainer();
        $model = $container->getModel();
        $module = $model->hasModule($app);

        // No module found
        if ($module === null) {
            return new Response('Forbidden', 403);
        }

        /*
         * If it's the default module and the project is published
         * or there are some specific permissions applied on the module
         */
        if (($module->getSlug() === null && $project->isPublished())
            || $this->hasModuleRoutePermission($module, $uri)) {
            $this->updateTimeAccess($container);
            $this->setRouteInCache($app, $slug, $uri);
            return new Response('OK', 200);
        // Else if the user is logged and be part of the project
        } elseif ($authorizationChecker->isGranted('ROLE_USER')) {
            if (!$project->hasUser($this->getUser())) {
                return new Response('Forbidden', 403);
            }

            $this->updateTimeAccess($container);
            $this->setRouteInCache($app, $slug, $uri);
            return new Response('OK', 200);
        }

        // Used to redirect back the user after login
        $this->get('session')->set('loginReferer', $domain . $uri);

        return new Response('Unauthorized', 401);
    }
    
    /**
     * Returns the subdomains of a given host name.
     *
     * @param string The host name
     *
     * @return array The subdomains
     */
    private function getSubDomains(string $domain)
    {
        $baseDomain = $this->getParameter('app.domain');

        if ($baseDomain === $domain) {
            return [];
        }

        $pos = strpos($domain, $baseDomain);
        return explode('-', substr($domain, 0, $pos - 1));
    }

    /**
     * Checks if a given module has some specific permission on a uri.
     *
     * @param Module $module The module to check
     * @param string $uri The uri to check
     *
     * @return Boolean The rule existence
     */
    private function hasModuleRoutePermission(Module $module, string $uri)
    {
        foreach ($module->getPermissions() as $permission) {
            if ($permission->isRegex()) {
                if (preg_match($permission->getRule(), $uri) === 1) {
                    return true;
                }
            } else {
                if ($uri === $permission->getRule()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Checks if a route is cached.
     *
     * @param string|null $app The app
     * @param string $slug The slug
     * @param string $uri The uri
     *
     * @return Boolean The route existence
     */
    private function isRouteCached($app, string $slug, string $uri)
    {
        $cache = $this->get('session')->get('cachedRoutes', []);

        if (isset($cache[$app][$slug][$uri])) {
            $timeout = new \DateTime('now');
            // Caching for 10 minutes
            $timeout = $timeout->sub(new \DateInterval('PT10M'));

            if ($timeout > $cache[$app][$slug][$uri]) {
                unset($cache[$app][$slug][$uri]);
                $this->get('session')->set('cachedRoutes', $cache);
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * Sets a route in the cache.
     *
     * @param string|null $app The app
     * @param string $slug The slug
     * @param string $uri The uri
     */
    private function setRouteInCache($app, string $slug, string $uri)
    {
        $cache = $this->get('session')->get('cachedRoutes', []);
        $cache[$app][$slug][$uri] = new \DateTime('now');
        $this->get('session')->set('cachedRoutes', $cache);
    }

    /**
     * Updates the last time access of a container.
     *
     * @param Container $container The container to update
     */
    private function updateTimeAccess(Container $container)
    {
        $em = $this->getDoctrine()->getManager();

        if ($container->isRunning() === false) {
            $old_path = getcwd();
            chdir('/usr/share/keep/scripts');
            echo shell_exec('sudo -u keep /usr/share/keep/scripts/start_ct.sh '.$container->getId());
            chdir($old_path);
        }

        $container->setLastAccess(new \DateTime('now'));
        $container->setRunning(true);
        $em->persist($container);
        $em->flush($container);
    }
}
