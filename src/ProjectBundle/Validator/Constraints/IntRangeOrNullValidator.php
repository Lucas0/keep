<?php

namespace ProjectBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\RangeValidator;

class IntRangeOrNullValidator extends RangeValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$value || empty($value)) {
            return true;
        }

        if (!is_array($value) || count($value) != 2 || $value[0] > $value[1]) {
            $this->context->buildViolation('Invalid range')
                ->addViolation();
            return;
        }

        parent::validate($value[0], $constraint);
        parent::validate($value[1], $constraint);
    }
}
