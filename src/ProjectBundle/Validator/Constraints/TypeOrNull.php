<?php

namespace ProjectBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraints\Type;

/**
 * @Annotation
 */
class TypeOrNull extends Type
{
}
