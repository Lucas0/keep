<?php

namespace ProjectBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\TypeValidator;

class TypeOrNullValidator extends TypeValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$value || empty($value)) {
            return true;
        }

        parent::validate($value, $constraint);
    }
}
