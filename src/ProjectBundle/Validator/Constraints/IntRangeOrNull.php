<?php

namespace ProjectBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraints\Range;

/**
 * @Annotation
 */
class IntRangeOrNull extends Range
{
}
