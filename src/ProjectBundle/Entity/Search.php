<?php

namespace ProjectBundle\Entity;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use ProjectBundle\Validator\Constraints as ProjectAssert;

class Search
{
    private $keywords;

    /**
     * @Assert\All({
     *     @ProjectAssert\TypeOrNull(type="numeric"),
     * })
     */
    private $categories;

    /**
     * @ProjectAssert\IntRangeOrNull(min="2000", max="3000"),
     */
    private $year;

    /**
     * @ProjectAssert\IntRangeOrNull(min="2", max="4")
     */
    private $grade;

    /**
     * @ProjectAssert\TypeOrNull(type="numeric"),
     */
    private $campus;

    public function __construct(Request $request, $keywords)
    {
        $fields = $this->getFields();

        foreach ($fields as $field) {
            if ($field == "keywords") {
                $this->setKeywords($keywords);
                continue;
            }

            $req = $request->query->get($field);

            call_user_func_array([$this, 'set' . ucfirst($field)], [$req]);
        }
    }

    /**
     * Set keywords
     *
     * @param string keywords
     *
     * @return Search
     */
    public function setKeywords($keywords)
    {
        $this->keywords = explode(' ', $keywords);

        return $this;
    }

    /**
     * Get keywords
     *
     * @return array
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set categories id
     *
     * @param string categories
     *
     * @return Search
     */
    public function setCategories($categories)
    {
        if ($categories === null || empty($categories)) {
            $this->categories = null;
        } else {
            $this->categories = explode(',', $categories);
        }

        return $this;
    }

    /**
     * Get categories id
     *
     * @return     array
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set year range
     *
     * @param string yearRange
     *
     * @return Search
     */
    public function setYear($year)
    {
        if ($year === null || empty($year)) {
            $this->year = null;
        } else {
            $this->year = explode(',', $year);
        }

        return $this;
    }

    /**
     * Get year range
     *
     * @return array
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set grade range
     *
     * @param string grade
     *
     * @return Search
     */
    public function setGrade($grade)
    {
        if ($grade === null || empty($grade)) {
            $this->grade = null;
        } else {
            $this->grade = explode(',', $grade);
        }

        return $this;
    }

    /**
     * Get grade range
     *
     * @return array
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set campus id
     *
     * @param string campus
     *
     * @return Search
     */
    public function setCampus($campus)
    {
        if ($campus === null || empty($campus)) {
            $this->campus = null;
        } else {
            $this->campus = $campus;
        }

        return $this;
    }

    /**
     * Get campus id
     *
     * @return array
     */
    public function getCampus()
    {
        return $this->campus;
    }

    /**
     * Gets the fields of the search
     *
     * @return array
     */
    public function getFields()
    {
        return array_keys(get_object_vars($this));
    }

    /**
     * Gets the filled fields of the search
     *
     * @return array
     */
    public function getFilledFields()
    {
        $fields = $this->getFields();
        $filled = [];
        foreach ($fields as $field) {
            if (!empty($this->$field)) {
                array_push($filled, $field);
            }
        }

        return $filled;
    }
}
