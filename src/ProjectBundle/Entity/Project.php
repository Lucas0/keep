<?php

namespace ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use UserBundle\Entity\User;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="ProjectBundle\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="smallint", nullable=false)
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=20, nullable=false, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="miniature_path", type="string", length=120, nullable=true)
     */
    private $miniaturePath;

    /**
     * @var boolean
     *
     * @ORM\Column(name="published", type="boolean", nullable=false)
     */
    private $published = false;

    /**
     * @var string
     *
     * @ORM\Column(name="report_path", type="string", length=120, nullable=true)
     */
    private $reportPath;

    /**
     * @var \Collection
     *
     * @ORM\ManyToOne(targetEntity="Collection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="collection_id", referencedColumnName="id")
     * })
     */
    private $collection;

    /**
     * @var \Type
     *
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $type;

    /**
     * @var \ProjectUser
     *
     * @ORM\OneToMany(targetEntity="ProjectUser", mappedBy="project", cascade={"persist", "remove"})
     */
    private $members;

    /**
     * @var \Category
     *
     * @ORM\ManyToMany(targetEntity="Category")
     */
    private $categories;

    /**
     * @var \Campus
     *
     * @ORM\ManyToMany(targetEntity="Campus")
     */
    private $campus;

    /**
     * @var \Type
     *
     * @ORM\ManyToOne(targetEntity="ContainerBundle\Entity\Container", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="container_id", referencedColumnName="id")
     * })
     */
    private $container;

    public function __construct()
    {
        $this->members = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return Project
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Project
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set miniaturePath
     *
     * @param string $miniaturePath
     *
     * @return Project
     */
    public function setMiniaturePath($miniaturePath)
    {
        $this->miniaturePath = $miniaturePath;

        return $this;
    }

    /**
     * Get miniaturePath
     *
     * @return string
     */
    public function getMiniaturePath()
    {
        return $this->miniaturePath;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return Project
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * Set reportPath
     *
     * @param string $reportPath
     *
     * @return Project
     */
    public function setReportPath($reportPath)
    {
        $this->reportPath = $reportPath;

        return $this;
    }

    /**
     * Get reportPath
     *
     * @return string
     */
    public function getReportPath()
    {
        return $this->reportPath;
    }

    /**
     * Set lastAccess
     *
     * @param \DateTime $lastAccess
     *
     * @return Project
     */
    public function setLastAccess(\DateTime $lastAccess = null)
    {
        $this->lastAccess = $lastAccess;

        return $this;
    }

    /**
     * Get lastAccess
     *
     * @return \DateTime
     */
    public function getLastAccess()
    {
        return $this->lastAccess;
    }

    /**
     * Set collection
     *
     * @param \ProjectBundle\Entity\Collection $collection
     *
     * @return Project
     */
    public function setCollection(\ProjectBundle\Entity\Collection $collection = null)
    {
        $this->collection = $collection;

        return $this;
    }

    /**
     * Get collection
     *
     * @return \ProjectBundle\Entity\Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Set type
     *
     * @param \ProjectBundle\Entity\Type $type
     *
     * @return Project
     */
    public function setType(\ProjectBundle\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \ProjectBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    public function hasUser(User $user)
    {
        $members = $this->getMembers();

        foreach ($members as $member) {
            if ($member->getUser()->getId() == $user->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Add member
     *
     * @param \ProjectBundle\Entity\ProjectUser $member
     *
     * @return Project
     */
    public function addMember(\ProjectBundle\Entity\ProjectUser $member)
    {
        $this->members[] = $member;

        return $this;
    }

    /**
     * Get members
     *
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Remove member
     *
     * @param \ProjectBundle\Entity\ProjectUser $member
     */
    public function removeMember(\ProjectBundle\Entity\ProjectUser $member)
    {
        $this->members->removeElement($member);
    }

    /**
     * Add categories
     *
     * @param \ProjectBundle\Entity\Category $categories
     *
     * @return Project
     */
    public function addCategory(\ProjectBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \ProjectBundle\Entity\Category $categories
     */
    public function removeCategory(\ProjectBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set campus
     *
     * @param Doctrine\Common\Collections\ArrayCollection $campus
     *
     * @return Project
     */
    public function setCampus(ArrayCollection $campus)
    {
        $this->campus = $campus;

        return $this;
    }

    /**
     * Add campus
     *
     * @param \ProjectBundle\Entity\Campus $campus
     *
     * @return Project
     */
    public function addCampus(\ProjectBundle\Entity\Campus $campus)
    {
        $this->campus[] = $campus;

        return $this;
    }

    /**
     * Remove campus
     *
     * @param \ProjectBundle\Entity\Campus $campus
     */
    public function removeCampus(\ProjectBundle\Entity\Campus $campus)
    {
        $this->campus->removeElement($campus);
    }

    /**
     * Get campus
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampus()
    {
        return $this->campus;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set container
     *
     * @param \ContainerBundle\Entity\Container $container
     *
     * @return Project
     */
    public function setContainer(\ContainerBundle\Entity\Container $container = null)
    {
        $this->container = $container;

        return $this;
    }

    /**
     * Get container
     *
     * @return \ContainerBundle\Entity\Container
     */
    public function getContainer()
    {
        return $this->container;
    }
}
