<?php

namespace ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Campus
 *
 * @ORM\Table(name="campus", indexes={@ORM\Index(name="fk_campus_city_idx", columns={"city_id"}), @ORM\Index(name="fk_campus_school_idx", columns={"school_id"})})
 * @ORM\Entity(repositoryClass="ProjectBundle\Repository\CampusRepository")
 */
class Campus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var \City
     *
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;

    /**
     * @var \School
     *
     * @ORM\ManyToOne(targetEntity="School")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_id", referencedColumnName="id")
     * })
     */
    private $school;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Campus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set city
     *
     * @param \ProjectBundle\Entity\City $city
     *
     * @return Campus
     */
    public function setCity(\ProjectBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \ProjectBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set school
     *
     * @param \ProjectBundle\Entity\School $school
     *
     * @return Campus
     */
    public function setSchool(\ProjectBundle\Entity\School $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \ProjectBundle\Entity\School
     */
    public function getSchool()
    {
        return $this->school;
    }

    public function __toString()
    {
        return $this->getSchool() . ' ' . $this->getCity() . (is_string($this->name) ? ' ' . $this->name : '');
    }
}
