<?php

namespace ProjectBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CampusRepository extends EntityRepository
{
    /**
     * Returns all the campuses.
     *
     * @return array The campuses
     */
    public function getCampuses()
    {
        $repo = $this->getEntityManager()->getRepository('ProjectBundle:Campus');

        return $repo->createQueryBuilder('c')->select('c.id, c.name, s.name AS school, ci.name as city')
            ->join('c.school', 's')
            ->join('c.city', 'ci')
            ->getQuery()
            ->getArrayResult();
    }
}
