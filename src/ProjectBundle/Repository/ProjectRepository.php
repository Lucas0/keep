<?php

namespace ProjectBundle\Repository;

use Doctrine\ORM\EntityRepository;
use UserBundle\Entity\User;
use ProjectBundle\Entity\Search;

class ProjectRepository extends EntityRepository
{
    /**
     * Returns a pre-created request that can be customized.
     *
     * @return QueryBuilder The query
     */
    private function getBaseRequest()
    {
        return $this->createQueryBuilder('p')
        ->select('p, m, c, t, u, r, cat, campus, city, school, container, model, modules')
        ->join('p.members', 'm')
        ->leftJoin('p.collection', 'c')
        ->join('p.type', 't')
        ->join('m.user', 'u')
        ->join('m.role', 'r')
        ->leftJoin('p.categories', 'cat')
        ->join('p.campus', 'campus')
        ->join('campus.city', 'city')
        ->join('campus.school', 'school')
        ->leftJoin('p.container', 'container')
        ->leftJoin('container.model', 'model')
        ->leftJoin('model.modules', 'modules')
        ->leftJoin('modules.permissions', 'module')
        ->orderBy('p.id');
    }

    /**
     * Returns the raw year of a project from a date.
     *
     * @param \DateTime $date The date
     *
     * @return int The corresponding year
     */
    private function getProjectYearFromDate(\DateTime $date)
    {
        $year = $date->format('Y');
        $month = $date->format('m');

        if ($month >= 9) {
            return $year;
        }

        return $year - 1;
    }

    /**
     * Returns a project based on his id.
     *
     * @param int $id The id
     *
     * return Array|null The project or null if not found
     */
    public function getProject(int $id)
    {
        $res = $this->getBaseRequest()
        ->where('p.id = :id')
        ->setParameter('id', $id)
        ->getQuery()
        ->getResult();

        if (count($res) > 0) {
            return $res[0];
        }

        return null;
    }

    /**
     * Returns a project based on his slug.
     *
     * @param string $slug The slug
     *
     * return Array|null The project or null if not found
     */
    public function getProjectBySlug(string $slug)
    {
        $res = $this->getBaseRequest()
        ->where('p.slug = :slug')
        ->setParameter('slug', $slug)
        ->getQuery()
        ->getResult();

        if (count($res) > 0) {
            return $res[0];
        }

        return null;
    }

    /**
     * Returns the user projects of the current year.
     *
     * @param User $user The user
     *
     * @return array The results
     */
    public function getActiveProjectsFromUser(User $user)
    {
        return $this->getBaseRequest()
        ->where('u.id = :id')
        ->setParameter('id', $user->getId())
        ->andWhere('p.year = :year')
        ->setParameter('year', $this->getProjectYearFromDate(new \DateTime('now')))
        ->andWhere('r.name = :student')
        ->setParameter('student', 'student')
        ->getQuery()
        ->getArrayResult();
    }

    /**
     * Returns all the projects of a user.
     *
     * @param User $user The user
     *
     * @return array The results
     */
    public function getProjectsFromUser(User $user)
    {
        return $this->getBaseRequest()
        ->where('u.id = :id')
        ->setParameter('id', $user->getId())
        ->andWhere('r.name = :student')
        ->setParameter('student', 'student')
        ->orderBy('p.year', 'DESC')
        ->getQuery()
        ->getArrayResult();
    }

    /**
     * Returns the last projects inserted in the database.
     *
     * @param int $limit The number of results
     * @param array $order The order parameter
     *
     * @return array The results
     */
    public function getLastProjects(int $limit = 50, array $order = ['id', 'ASC'])
    {
        return $this->getBaseRequest()
        ->getQuery()
        ->getArrayResult();
    }

    /**
     * Searches projects from the database.
     *
     * @param Search $search The search entity
     * @param int $limit The number of results
     * @param int $offset The offset in the search result
     *
     * @return array The results
     */
    public function search(Search $search, int $limit = 5, int $offset = 0)
    {
        $searchFields = $search->getFilledFields();
        
        $query = $this->getBaseRequest()
        ->orderBy('p.id');


        if (in_array('keywords', $searchFields)) {
            $nbKeyword = count($search->getKeywords());
            for ($i=0; $i < $nbKeyword; ++$i) {
                if (empty($search->getKeywords()[$i])) {
                    continue;
                }

                $query->andWhere('p.name LIKE :keyword_'.$i);
                $query->orWhere('u.firstname LIKE :keyword_'.$i);
                $query->orWhere('u.lastname LIKE :keyword_'.$i);
                $query->orWhere('p.description LIKE :keyword_'.$i);
                $query->setParameter('keyword_'.$i, '%'.$search->getKeywords()[$i].'%');
            }
        }

        if (in_array('year', $searchFields)) {
            $query->andWhere('p.year >= :down_year');
            $query->andWhere('p.year <= :up_year');

            $query->setParameter('down_year', $search->getYear()[0]);
            $query->setParameter('up_year', $search->getYear()[1]);
        }

        if (in_array('grade', $searchFields)) {
            $query->andWhere('m.grade >= :down_grade OR r.name = :tutor');
            $query->andWhere('m.grade <= :up_grade');

            $query->setParameter('tutor', 'tutor');
            $query->setParameter('down_grade', $search->getGrade()[0]);
            $query->setParameter('up_grade', $search->getGrade()[1]);
        }
        
        if (in_array('categories', $searchFields)) {
            $query->andWhere('cat.id IN(:cats)')
            ->setParameter('cats', $search->getCategories());
        }

        if (in_array('campus', $searchFields)) {
            $query->andWhere('campus.id = :campus')
            ->setParameter('campus', $search->getCampus());
        }

        return $query->getQuery()->getArrayResult();
    }
}
