<?php

namespace ProjectBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    /**
     * Returns all the categories.
     *
     * @return array The categories
     */
    public function getCategories()
    {
        $repo = $this->getEntityManager()->getRepository('ProjectBundle:Category');

        return $repo->createQueryBuilder('c')
            ->select('c.id, c.name')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * Returns categories matching some keywords.
     *
     * @param string $search The keywords
     *
     * @return array The results
     */
    public function searchCategories(string $search)
    {
        $repo = $this->getEntityManager()->getRepository('ProjectBundle:Category');

        return $repo->createQueryBuilder('c')
            ->select('c.id, c.name')
            ->where('c.name LIKE :search')
            ->setParameter('search', '%'.$search.'%')
            ->getQuery()
            ->getArrayResult();
    }
}
