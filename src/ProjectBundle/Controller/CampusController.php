<?php

namespace ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use ProjectBundle\Entity\Campus;

/**
 * Campus management.
 *
 * @Security("is_granted('ROLE_ADMIN')")
 */
class CampusController extends Controller
{
    /**
     * Shows and adds campuses.
     *
     * @return Response A Response instance
     */
    public function campusesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->request->has('campus')) {
            $campus = new Campus();
            $campus->setName($request->request->get('campus'));
            $em->persist($campus);
            $em->flush();

            return $this->redirect($this->generateUrl('project_campuses'));
        }

        $campuses = $em->getRepository('ProjectBundle:Campus')->getCampuses();

        return $this->render('ProjectBundle:Campus:campuses.html.twig', compact('campuses'));
    }

    /**
     * Removes a campus.
     *
     * @return Response A Response Instance
     *
     * @throws HttpException
     */
    public function removeAction(Request $request, int $id)
    {
        $token = $request->request->get('_csrf_token');
        $csrf_token = new CsrfToken('delete_campus', $token);
        if (!$this->get('security.csrf.token_manager')->isTokenValid($csrf_token)) {
            throw new HttpException(500, "Invalid token.");
        }

        $em = $this->getDoctrine()->getManager();

        $campus = $em->getRepository('ProjectBundle:Campus')->find($id);

        if ($campus !== null) {
            $em->remove($campus);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('project_campuses'));
    }
}

