<?php

namespace ProjectBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use ProjectBundle\Entity\Project;
use ProjectBundle\Entity\ProjectUser;
use ProjectBundle\Entity\Search;
use ContainerBundle\Entity\Container;

/**
 * Project controller.
 */
class ProjectController extends Controller
{
    /**
     * Creates a new project entity.
     *
     * @param Request $request
     *
     * @return Response A Response instance
     *
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $project = new Project();
        $form = $this->createForm('ProjectBundle\Form\Type\ProjectType', $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $role = $em->getRepository('UserBundle:Role');
            $roles = $role->findAll();

            $users = $request->request->get('projectbundle_users');
            $this->projectUserHandle($users, $project, $roles, $em);

            $container = new Container();
            $container->setLastAccess(null);

            // Using default model
            $modelRepo = $em->getRepository('ContainerBundle:ContainerModel');
            $model = $modelRepo->findOneByName('nest');
            $container->setModel($model);

            $containerRepo = $em->getRepository('ContainerBundle:Container');
            $container->setIp($containerRepo->getNextIp());
            $project->setContainer($container);

            $em->persist($project);
            $em->flush($project);

            $this->createContainer($container->getId(), $container->getIp(), $project->getSlug(), 'nest');

            return $this->redirectToRoute('project_edit', ['id' => $project->getId()]);
        }

        return $this->render('ProjectBundle:Project:edit.html.twig', array(
            'project' => $project,
            'edit_form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing project entity.
     *
     * @param Request $request
     * @param Project $project The project to edit
     *
     * @return Response A Response instance
     *
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function editAction(Request $request, Project $project)
    {
        $em = $this->getDoctrine()->getManager();
        if ($project->getContainer() !== null) {
            $deleteFormView = $this->createDeleteForm($project)->createView();
        } else {
            $deleteFormView = null;
        }
        $editForm = $this->createForm('ProjectBundle\Form\Type\ProjectType', $project);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $users = $request->request->get('projectbundle_users');
            $this->projectUserHandle($users, $project, $em);

            $em->persist($project);
            $em->flush();

            return $this->redirectToRoute('project_edit', array('id' => $project->getId()));
        }

        return $this->render('ProjectBundle:Project:edit.html.twig', array(
            'project' => $project,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteFormView,
        ));
    }

    /**
     * Deletes a project container.
     *
     * @param Request $request
     * @param Project The project to delete
     *
     * @return Response A Response instance
     *
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, Project $project)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createDeleteForm($project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->destroyContainer($project);

            $project->setContainer(null);

            $em->persist($project);
            $em->flush($project);
        }

        return new JsonResponse(['error' => false]);
    }

    /**
     * Search JSON API for projects.
     *
     * @param Request $request
     * @param string $search The search keywords
     *
     * @throws NotFoundHttpException
     *
     * @return Response A Response instance
     */
    public function searchAction(Request $request, string $search = '')
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('ProjectBundle:Project');

        // If there is no selected filters, get last projects
        if (strlen($search) == 0 && $request->query->count() == 0) {
            $projects = $repo->getLastProjects();
        } else {
            // Checking user input before doing the search
            $res = new Search($request, $search);
            $validator = $this->get('validator');
            $errors = $validator->validate($res);

            if (count($errors) > 0) {
                throw $this->createNotFoundException('Invalid search.');
            }

            $projects = $repo->search($res);
        }

        return new JsonResponse($projects);
    }

    /**
     * Updates the project users from a form.
     *
     * @param array $form The form where to retrieve user informations
     * @param Project project The projet to update
     */
    private function projectUserHandle(array $form, Project $project)
    {
        $em = $this->getDoctrine()->getManager();
        $roles = $em->getRepository('UserBundle:Role')->findAll();
        $userRepo = $em->getRepository('UserBundle:User');

        $registeredUsers = [];

        foreach ($project->getMembers() as $member) {
            $registeredUsers[$member->getId()] = $member;
        }
        $registeredUsersIds = array_keys($registeredUsers);

        foreach ($form as $user) {
            // If the user is in the database and in the form
            if (($key = array_search($user['id'], $registeredUsersIds)) !== false) {
                $registeredUsers[$user['id']]->setGrade($user['grade']);
                // Removing from the registered users ids
                array_splice($registeredUsersIds, $key, 1);
            } else {
                // Adding the user to the project
                $dbUser = $userRepo->find($user['id']);
                if ($dbUser !== null) {
                    $project_user = new ProjectUser();
                    $project_user
                        ->setUser($dbUser)
                        ->setProject($project)
                        ->setGrade((int) $user['grade'])
                        ->setRole($roles[$user['role']])
                    ;
                    $project->addMember($project_user);
                }
            }
        }

        // Registered users left are the removed one from the form
        foreach ($registeredUsersIds as $id) {
            $project->removeMember($registeredUsers[$id]);
            $em->remove($registeredUsers[$id]);
        }
    }

    /**
     * Creates a form to delete a project entity.
     *
     * @param Project $project The project entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Project $project)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('project_delete', array('id' => $project->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Creates a new project container.
     *
     * @param int $id Id of the new container
     * @param string $ip IP address of the new container
     * @param string $slug Slug of the project
     *
     * @throws HttpException
     */
    private function createContainer(int $id, string $ip, string $slug, string $model)
    {
        if (!preg_match('/([0-9]{1,3}\.){3}[0-9]{1,3}/', $ip)
            || !preg_match('/[a-z0-9-]+/', $slug)) {
            throw new HttpException(500, "Invalid parameters.");
        }

        $old_path = getcwd();
        chdir('/usr/share/keep/scripts');
        shell_exec('sudo -u keep /usr/share/keep/scripts/add_project.sh '.$id.' '.$ip.' '.$slug.' '.$model);
        chdir($old_path);
    }

    /**
     * Deletes a project container.
     *
     * @param Project $project The project that the container will be deleted
     *
     * @throws HttpException
     */
    private function destroyContainer(Project $project)
    {
        if ($project->getId() === null || $project->getContainer() === null) {
            throw new HttpException(500, "Invalid request.");
        }
        $em = $this->getDoctrine()->getManager();
        
        $project->setContainer(null);
        $em->persist($project);
        $em->flush($project);

        $old_path = getcwd();
        chdir('/usr/share/keep/scripts');
        shell_exec('sudo -u keep /usr/share/keep/scripts/del_project.sh '.$project->getId());
        chdir($old_path);
    }
}
