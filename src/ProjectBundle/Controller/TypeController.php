<?php

namespace ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use ProjectBundle\Entity\Type;

/**
 * Type management.
 *  
 * @Security("is_granted('ROLE_ADMIN')")
 */
class TypeController extends Controller
{
    /**
     * Shows and adds types.
     *
     * @return Response A Response instance
     */
    public function typesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $type = new Type();

        $form = $this->createFormBuilder($type)
            ->add('name', TextType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($type);
            $em->flush();

            return $this->redirect($this->generateUrl('project_types'));
        }

        $types = $em->getRepository('ProjectBundle:Type')->findAll();


        return $this->render('ProjectBundle:Type:types.html.twig', [
            'types'    => $types,
            'form'     => $form->createView()
        ]);
    }

    /**
     * Removes a type.
     *
     * @return Response A Response Instance
     *
     * @throws HttpException
     */
    public function removeAction(Request $request, int $id)
    {
        $token = $request->request->get('_csrf_token');
        $csrf_token = new CsrfToken('delete_type', $token);
        if (!$this->get('security.csrf.token_manager')->isTokenValid($csrf_token)) {
            throw new HttpException(500, "Invalid token.");
        }

        $em = $this->getDoctrine()->getManager();

        $type = $em->getRepository('ProjectBundle:Type')->find($id);

        if ($type !== null) {
            $em->remove($type);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('project_types'));
    }

}

