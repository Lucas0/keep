<?php

namespace ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use ProjectBundle\Entity\Category;

/**
 * Category management.
 *
 * @Security("is_granted('ROLE_ADMIN')")
 */
class CategoryController extends Controller
{
    /**
     * Category search JSON API for categories.
     *
     * @param Request $request
     * @param string $search The search keywords
     *
     * @return JsonResponse A JsonResponse instance
     */
    public function searchAction(Request $request, string $search = '')
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('ProjectBundle:Category');

        if (strlen($search) > 0) {
            $res = $repo->searchCategories($search);
        } else {
            $res = $repo->getCategories();
        }

        return new JsonResponse($res);
    }

    /**
     * Shows and adds categories.
     *
     * @return Response A Response instance
     */
    public function categoriesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $category = new Category();

        $form = $this->createFormBuilder($category)
            ->add('name', TextType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($category);
            $em->flush();

            return $this->redirect($this->generateUrl('project_categories'));
        }

        $categories = $em->getRepository('ProjectBundle:Category')->getCategories();

        return $this->render('ProjectBundle:Category:categories.html.twig', [
            'categories'    => $categories,
            'form'          => $form->createView()
        ]);
    }

    /**
     * Removes a category.
     *
     * @return Response A Response Instance
     *
     * @throws HttpException
     */
    public function removeAction(Request $request, int $id)
    {
        $token = $request->request->get('_csrf_token');
        $csrf_token = new CsrfToken('delete_category', $token);
        if (!$this->get('security.csrf.token_manager')->isTokenValid($csrf_token)) {
            throw new HttpException(500, "Invalid token.");
        }

        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('ProjectBundle:Category')->find($id);

        if ($category !== null) {
            $em->remove($category);
            $em->flush();
        }

        return $this->redirectToRoute('project_categories');
    }
}

