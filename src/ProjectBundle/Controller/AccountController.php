<?php

namespace ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Controller managing the user projects.
 *
 * @Security("is_granted('ROLE_USER')")
 */
class AccountController extends Controller
{
    /**
     * User homepage.
     *
     * @param Request $request
     *
     * @return Response A Response instance
     */
    public function indexAction(Request $request)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('ProjectBundle:Project');
        $projects = $repo->getProjectsFromUser($this->getUser());
        return $this->render('ProjectBundle:Account:index.html.twig', compact('projects'));
    }
    
    /**
     * The sidebar of the user.
     *
     * @param Request $request
     *
     * @return Response A Response instance
     */
    public function sidebarAction(Request $request)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('ProjectBundle:Project');
        $projects = $repo->getProjectsFromUser($this->getUser());
        return $this->render('ProjectBundle:Account:sidebar.html.twig', compact('projects'));
    }

    /**
     * The default page displayed on the user homepage.
     *
     * @param Request $request
     *
     * @return Response A Response instance
     */
    public function welcomeAction(Request $request)
    {
        return $this->render('ProjectBundle:Account:welcome.html.twig');
    }

    /**
     * Allows users to edit some properties of their project.
     *
     * @param Request $request
     * @param string $slug Slug of the project to edit
     *
     * @throws HttpException
     *
     * @return Response A Response instance
     */
    public function editAction(Request $request, string $slug)
    {
        $em = $this->getDoctrine()->getManager();

        $project = $em->getRepository('ProjectBundle:Project')->findOneBySlug($slug);

	if($project === null) {
            throw $this->createNotFoundException('The project does not exist.');
	} else if(!$project->hasUser($this->getUser())) {
            throw $this->createAccessDeniedException("You're not allowed to edit this project.");
	}

        $form = $this->createFormBuilder($project)
            ->add('description', TextareaType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($project);
            $em->flush($project);

            return $this->redirectToRoute('project_account_edit', ['slug' => $slug]);
        }

        return $this->render('ProjectBundle:Account:edit.html.twig', [
            "project" => $project,
            "form"    => $form->createView()
        ]);
    }
}
