<?php

namespace ProjectBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProjectType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true
            ])
            ->add('slug', TextType::class, [
                'required' => true
            ])
            ->add('description', TextareaType::class, [
                'required' => false
            ])
            ->add('year', IntegerType::class, [
                'required' => true
            ])
            ->add('published', CheckboxType::class, [
                'required' => false
            ])
            ->add('collection', EntityType::class, [
                'class'     => 'ProjectBundle:Collection',
                'required' => false
            ])
            ->add('type', EntityType::class, [
                'class'     => 'ProjectBundle:Type',
                'required' => true
            ])
            ->add('categories', EntityType::class, [
                'class'     => 'ProjectBundle:Category',
                'multiple'  => true,
                'required' => true
            ])
            ->add('campus', EntityType::class, [
                'class'     => 'ProjectBundle:Campus',
                'multiple'  => true,
                'required' => true
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ProjectBundle\Entity\Project'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'projectbundle_project';
    }
}
