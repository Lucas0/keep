function getMaxSearchYear() {
    var year = new Date().getFullYear();

    if(new Date().getMonth() + 1 >= 9) {
        ++year;
    }

    return year;
}

window.addEventListener('load', function() {
    var year = getMaxSearchYear();
    var fields = {
        '#search': {
            'alias': 'keywords'
        },
        '#campus': {},
        '#grade': {
            'type': 'range',
            'slider': new Slider("#grade", { min: 2, max: 4, value: [2, 4], focus: true })
        },
        '#year': {
            'type': 'range',
            'slider': new Slider("#year", { min: 2010, max: year, value: [2010, year] })
        },
        '.categoriescheck': {
            'type': 'checkbox',
            'alias': 'categories'
        }
    }

    var searchParser = new SearchFormParser(fields);
    var searchRenderer = new SearchHomeRenderer({
        'root': '#projects'
    });
    var search = new Search(searchParser,searchRenderer);
});
