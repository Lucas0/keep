/**
 * Search renderer for the admin interface
 *
 * @class      SearchAdminRenderer
 * @param      {array}  fields               The fields' html class and identifiers
 * @param      {class}  adminProjectManager  The admin project manager
 * @param      {array}  tr                   field translation for printing
 */
function SearchAdminRenderer(fields, adminProjectManager, tr) {
    SearchRenderer.apply(this, fields);
    if(fields === undefined) {
        console.error("fields is undefined");
        return;
    }


    this.root = document.getElementById(fields.root.slice(1));
    if(this.root === null) {
        console.error("root field not found");
        return;
    }

    this.adminProjectManager = adminProjectManager;
    this.tr = tr;
};

/**
 * Erases the search results
 */
SearchAdminRenderer.prototype.erase = function() {
    this.root.innerHTML = '';
}

/**
 * Inserts an item into the list
 *
 * @param      {array}  item    The item to append
 */
SearchAdminRenderer.prototype.insertItem = function(item) {
    var mainLi = document.createElement('li');

    var input = createNodeWithParams('input', {
        'type': 'checkbox',
        'class': 'checkon' + (this.adminProjectManager.isSelectionModeEnabled() ? '' : ' hide'),
        'data-id' : item.id,
        'data-name': item.name,
    });

    input.checked = (this.adminProjectManager.getSelectedById(item.id) >= 0 ? true : false);
    mainLi.appendChild(input);

    var a = createNodeWithParams('a', {
        'class': 'hide_info',
        'data-id' : item.id,
        'data-name': item.name,
        'innerHTML': item.name + ' ',
        'data-hasct': (item.container == null ? 'false' : 'true')
    });
    mainLi.appendChild(a);

    if(item.container == null) {
        var span_ip = createNodeWithParams('span', {
            'class': 'badge alert',
            'innerHTML': '<i class="fi-x"></i>',
            'title': 'Projet supprimé',
        });
        a.appendChild(span_ip);
    }

    var mainUl = createNodeWithParams('ul', {
        'class': 'list',
        'style': 'display: none;'
    });
    mainLi.appendChild(mainUl);

    var students = [];
    var tutors = [];

    if(item.members !== null) {
        for(m in item.members) {
            if(item.members[m].role.name == "student") {
                students.push(item.members[m].user.lastname + ' ' + item.members[m].user.firstname);
            } else if(item.members[m].role.name == "tutor") {
                tutors.push(item.members[m].user.lastname + ' ' + item.members[m].user.firstname);
            }
        }
    }

    for (field in item) {
        if(field in {'year':'', 'location': '','categories': ''}) {
            var li = createNodeWithParams('li', {
                innerHTML: '<strong>'+ this._(field) +'</strong> : '
            });

            if(item[field] != null) {
                if(typeof item[field] == "object") {
                    li.innerHTML += item[field].map(function(v) {return v.name;}).join(', ');
                } else {
                    li.innerHTML += item[field];
                }
            }

            mainUl.appendChild(li);
        }
    }

    for(field in {'students':'', 'tutors':''}) {
        var li = createNodeWithParams('li', {
            innerHTML: '<strong>'+ this._(field) +'</strong> : '
        });

        if(field == 'students') {
            li.innerHTML += students.join(', ');
        } else {
            li.innerHTML += tutors.join(', ');
        }

        mainUl.appendChild(li);
    }

    this.root.appendChild(mainLi);

    this.adminProjectManager.watch({
        id: item.id,
        name: item.name,
        hasct: (item.container == null ? false : true),
        checkbox: input,
        description: a
    })
}

/**
 * Calls insertItem for an array of item
 *
 * @param      {array}  items   The items
 */
SearchAdminRenderer.prototype.insertItems = function(items) {
    for(k in items) {
        this.insertItem(items[k]);
    }
}

/**
 * Do the field translation
 *
 * @param      {string}  field   The field
 * @return     {string}  translated field if found, field otherwise
 */
SearchAdminRenderer.prototype._ = function(field) {
    if(this.tr[field] !== undefined) {
        return this.tr[field];
    }

    return field;
}

Object.setPrototypeOf(SearchAdminRenderer, SearchRenderer);