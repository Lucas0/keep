// Real-time server informations
function getInformation() {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', '/admin/serveur/informations');
	xhr.addEventListener('readystatechange', function() {
		if(xhr.readyState == 4 && xhr.status == 200) {
			var info = JSON.parse(xhr.response);
			document.getElementById('memory').value = info.mem;
			document.getElementById('memoryShow').innerHTML = Math.round(info.mem) + " %";
			document.getElementById('Cpu').value = Math.round(info.cpu*100)*0.25;
			document.getElementById('CpuShow').innerHTML = Math.round(info.cpu*100)*0.25 + " %";
		}
	});

	xhr.send();
	setTimeout(getInformation, 2000);
}

var tr = {
    'name': 'Titre',
    'description': 'Description',
    'grade': 'Année',
    'year': 'Année universitaire',
    'students': 'Étudiants',
    'location': 'Campus',
    'tutors': 'Suiveurs'
};

function subMenuBut() {
    document.getElementById("myDropdown").classList.toggle("show");
}

function getMaxSearchYear() {
    var year = new Date().getFullYear();

    if(new Date().getMonth() + 1 >= 9) {
        ++year;
    }

    return year;
}

window.addEventListener('load', function() {
    var year = getMaxSearchYear();
    var fields = {
        '#search': {
            'alias': 'keywords'
        },
        '#campus': {},
        '#grade': {
            'type': 'range',
            'slider': new Slider("#grade", { min: 2, max: 4, value: [2, 4], focus: true })
        },
        '#year': {
            'type': 'range',
            'slider': new Slider("#year", { min: 2010, max: year, value: [2010, year] })
        },
        '.categoriescheck': {
            'type': 'checkbox',
            'alias': 'categories'
        }
    }

    var searchParser = new SearchFormParser(fields);
    var adminProjectManager = new AdminProjectManager({
        'root': '#projects',
        'remove': '#deletebtn',
        'edit': '#editbtn',
        'select': '#modifybtn',
        'selectArea': '#Nestselect'
    });
    var searchRenderer = new SearchAdminRenderer(
        {'root': '#projects'},
        adminProjectManager,
        tr
    );
    var search = new Search(searchParser,searchRenderer);
    search.onUpdate();

    getInformation();

    // Tab filters/categories management
    function updateButtons() {
        changecolor();
        showinformation();
    }

    function changecolor() {
        div.style.height = "150px";
        if(!buttons.filters) {
            Filterb.style.backgroundColor="#4AAAC1";
            if(!buttons.categories) {
              div.style.height = "2px"; 
            }
        } else {
            Filterb.style.backgroundColor="#258dad";
        }

        if(buttons.categories) {
            Categoriesb.style.backgroundColor="#258dad";
        } else {
            Categoriesb.style.backgroundColor="#4AAAC1";
        }
    };

    function showinformation() {
        categoriesBlock.style.display = buttons.categories ? 'block' : 'none';
        filtersBlock.style.display = buttons.filters ? 'block' : 'none';
    }

    var Categoriesb = document.getElementById('Categoriesbutton');
    var Filterb =document.getElementById('Filterbutton');
    var div = document.getElementById('FilterMenu');
    var categoriesBlock = document.getElementById('Categories');
    var filtersBlock = document.getElementById('Filter');

    var buttons = {filters: false, categories: false};


    Filterb.addEventListener('click', function() {
        buttons.filters = !buttons.filters;
        buttons.categories = false;

        updateButtons();
    });

    Categoriesb.addEventListener('click', function() {
        buttons.filters = false;
        buttons.categories = !buttons.categories;

        updateButtons();
    });

    var parametersMenu = document.getElementById('parametersmenu');
    parametersMenu.addEventListener('click', subMenuBut);

    // Close the dropdown menu if the user clicks outside of it
    window.addEventListener('click', function(event) {
        if (!event.target.matches('#parametersmenu')) {
            var dropdowns = document.getElementsByClassName("button-content");
            for (var i = 0; i < dropdowns.length; ++i) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    });

});
