/**
 * Creates node with parameters
 *
 * @param      {string}  nodeName  The node name
 * @param      {array}  params    The parameters
 * @return     {HtmlElement}  The generated node
 */
function createNodeWithParams(nodeName, params) {
	var div = document.createElement(nodeName);
	for(p in params) {
		if(p == "class") {
			div.className = params[p];
		} else if(p == "innerHTML") {
			div.innerHTML = params[p];
		} else {
			div.setAttribute(p, params[p]);
		}
	}

	return div;
}

/**
 * Gets the selected checkbox of an HTML input array
 *
 * @param      {array}  nodes   The nodes
 * @return     {array}   The selected checkbox
 */
function getSelectedCheckbox(nodes) {
	var res = [];

	for(var i = 0; i < nodes.length; ++i) {
		if(nodes[i].type != 'checkbox') continue;

		if(nodes[i].checked === true) {
			res.push(nodes[i].value);
		}
	}

	return res;
}

/**
 * Determines if an HTML element has the specified class.
 *
 * @param      {HtmlElement}   elem     The HTML Element
 * @param      {string}   cls     The class to test
 * @return     {boolean}  True if has the class, False otherwise.
 */
function hasClass(elem, cls) {
	return elem.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}

/**
 * Adds a class to an HTML element
 *
 * @param      {HtmlElement}  elem     The element
 * @param      {string}  cls     The class to add
 */
function addClass(elem, cls) {
	if (!this.hasClass(elem,cls)) elem.className += " "+cls;
}

/**
 * Removes a class to an HTML element
 *
 * @param      {HtmlElement}  elem     The element
 * @param      {string}  cls     The class to remove
 */
function removeClass(elem, cls) {
	if (hasClass(elem,cls)) {
    	var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		elem.className=elem.className.replace(reg,' ');
	}
}

/**
 * Toggles the text value of an HTML element with the dataset toggle element
 *
 * @param      {HtmlElement}  elem    The element
 */
function prependedToggleTextBtn(elem) {
	var oldValue = /(?:.+> )?(.+)$/.exec(elem.innerHTML)[1];
	elem.innerHTML = elem.innerHTML.replace(/(.+> )?.+$/, '$1' + elem.dataset.toggle);
	elem.dataset.toggle = oldValue;
}

/**
 * Converts a string into a short string (slug)
 *
 * @param      {string}  str    The string
 * @return     {string}  slug
 */
function convertToSlug(str)
{
	return str
        .toLowerCase()
        .replace(/[ _]/g,'-')
    	.replace(/[àáâãäå]/g,"a")
    	.replace(/[èéêëã]/g,"e")
    	.replace(/[îïíì]/g,"i")
    	.replace(/[úùûü]/g,"u")
    	.replace(/[ç]/g,"c")
    	.replace(/[óòôöõø]/g,"o")
    	.replace(/[æ]/g,"ae")
    	.replace(/[œ]/g,"oe")
    	.replace(/[ñ]/g,"n")
        .replace(/[^a-zA-Z0-9]+/g,'')
}

function toggleAttributes(elem, attr1, attr2) {
	if(elem.hasAttribute(attr1)) {
		if(elem.hasAttribute(attr2)) {
			var val = elem.getAttribute(attr1);
			elem.setAttribute(attr1, elem.getAttribute(attr2));
			elem.setAttribute(attr2, attr1);
		} else {
			elem.setAttribute(attr2, elem.getAttribute(attr1));
			elem.removeAttribute(attr1);
		}
	} else if(elem.hasAttribute(attr2)) {
		elem.setAttribute(attr1, elem.getAttribute(attr2));
		elem.removeAttribute(attr2);
	}
}