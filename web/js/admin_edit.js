window.addEventListener('load', function() {
    new LabelManager({
        area: document.getElementById('studentsarea'),
        placeholder: 'NOM Prénom...',
        formName: 'projectbundle_users[s%i][id]',
        xhrUrl: '/utilisateurs/s/%i',
        staticFields: [
            {
                fieldName: 'projectbundle_users[s%i][grade]',
                value: '2',
                class: 'project_student_grade'
            },
            {
                fieldName: 'projectbundle_users[s%i][role]',
                value: '0'
            }
        ],
        xhrMap: function(data, response) {
            var out = [];
            for(i in data) {
                out.push({
                    label: data[i].lastname + ' ' + data[i].firstname + ' (' + data[i].username + ')',
                    value: data[i].id
                });
            }
            response(out);
        }
    });

    new LabelManager({
        area: document.getElementById('tutorsarea'),
        placeholder: 'NOM Prénom...',
        formName: 'projectbundle_users[t%i][id]',
        xhrUrl: '/utilisateurs/s/%i',
        staticFields: [
            {
                fieldName: 'projectbundle_users[t%i][grade]',
                value: '0'
            },
            {
                fieldName: 'projectbundle_users[t%i][role]',
                value: '1'
            }
        ],
        xhrMap: function(data, response) {
            var out = [];
            for(i in data) {
                out.push({
                    label: data[i].lastname + ' ' + data[i].firstname + ' (' + data[i].username + ')',
                    value: data[i].id
                });
            }
            response(out);
        }
    });

    new LabelManager({
        area: document.getElementById('categoriesarea'),
        placeholder: 'Catégorie...',
        formName: 'projectbundle_project[categories][]',
        xhrUrl: '/projets/categories/s/%i',
        xhrMap: function(data, response) {
            var out = [];
            for(i in data) {
                out.push({label: data[i].name, value: data[i].id});
            }
            response(out);
        }
    });

    var slugField = document.getElementById('projectbundle_project_slug');
    document.getElementById('projectbundle_project_name').addEventListener('input', function() {
        slugField.value = convertToSlug(this.value).substr(0, 20);
    });

    var form = document.getElementById('project_form');
    form.addEventListener('submit', function(e) {
        var grade = document.getElementById('project_grade').value;
        var studentGrade = document.getElementsByClassName('project_student_grade');
        for (var i = 0; i < studentGrade.length; ++i) {
            studentGrade[i].value = grade;
        }
    });
});
