function AdminProject(fields) {
	this.fields = fields;
	this.events = {};

	var _this = this;
	fields.checkbox.addEventListener('change', function() {_this.triggerEvent('selected'); });
};

AdminProject.prototype.toggleDescription = function() {
	var list = this.fields.description.nextElementSibling;
	if(list.style.display=="none") 
	{ 
		list.style.display = "block";
	
		if(this.fields.description.dataset.hasct == "true") {
			deletebtn.removeAttribute("disabled");
		} else {
			deletebtn.setAttribute('disabled', "disabled");
		}
	} 
	else 
	{
		list.style.display = "none";
	}
}

AdminProject.prototype.toggleCheckboxValue = function() {
	this.fields.checkbox.checked = !this.fields.checkbox.checked;
}

AdminProject.prototype.toggleCheckbox = function() {
	if(hasClass(this.fields.checkbox, 'hide')) {
		removeClass(this.fields.checkbox, 'hide');
	} else {
		addClass(this.fields.checkbox, 'hide');
	}
}

AdminProject.prototype.addEventListener = function(event, callback) {
	this.events[event] = callback;
}

AdminProject.prototype.triggerEvent = function(event) {
	if(typeof this.events[event] == "function") {
		this.events[event].call(this.fields);
	}
}