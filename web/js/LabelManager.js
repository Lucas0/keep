function LabelManager(fields) {
    this.area = fields.area;
    this.placeholder = fields.placeholder;
    this.formName = fields.formName;
    this.labels = [];
    this.adder = null;
    this.fieldCount = 0;

    if(typeof fields.xhrUrl == "string") {
        this.ajax = true;
        this.xhrUrl = fields.xhrUrl;
        this.xhrMap = fields.xhrMap;
    } else {
        this.ajax = false;
    }

    if(typeof fields.staticFields == "object") {
        this.staticFields = fields.staticFields;
    } else {
        this.staticFields = [];
    }


    this.classNames = {
        item: 'label-item',
        deleter: 'label-delete',
        adder: 'label-item-add'
    };

    this.parse();
}

LabelManager.prototype.getFieldName = function(id, field = null) {
    if(field == null) {
        return this.formName.replace('%i', id);
    }
    return field.replace('%i', id);
}

LabelManager.prototype.getNextFieldId = function() {
    return this.fieldCount++;
}

LabelManager.prototype.appendField = function(label, type) {
    if(typeof label != 'object') {
        label = {text: '', value: ''};
    } else if(!label.hasOwnProperty('text')) {
        label['text'] = '';
    } else if(!label.hasOwnProperty('value')) {
        label['value'] = '';
    }

    fieldId = this.getNextFieldId();

    var wrap = createNodeWithParams('div', {
        class: this.classNames.item
    });

    var inputVisible = createNodeWithParams('input', {
        type: 'text',
        placeholder: this.placeholder,
        value: label.text
    });

    var inputValue = createNodeWithParams('input', {
        type: 'hidden',
        'data-name': this.getFieldName(fieldId),
        value: label.value
    });

    for (var i = 0; i < this.staticFields.length; ++i) {
        wrap.appendChild(
            createNodeWithParams('input', {
                type: 'hidden',
                'data-name': this.getFieldName(fieldId, this.staticFields[i].fieldName),
                value: this.staticFields[i].value,
                class: this.staticFields[i].class || ""
            })
        );
    }

    wrap.appendChild(inputVisible);
    wrap.appendChild(inputValue);

    if(label['value'].length > 0) {
        var inputs = wrap.getElementsByTagName('input');
        this.toggleInputsNames(inputs);
    }

    var postpend = createNodeWithParams('div', {
        class: this.classNames.deleter,
        innerHTML: (type == 'adder' ? '+' : '&#x2715;')
    });
    var _this = this;
    if(type == 'adder') {
        postpend.addEventListener('click', function() { _this.onAdd(inputVisible, inputValue); });
        this.adder = wrap;
    } else {
        postpend.addEventListener('click', function() { _this.remove(wrap); });
    }

    inputVisible.addEventListener('change', function() { _this.onInputChange(inputVisible) });

    wrap.appendChild(postpend);

    if(this.adder != null && type !== 'adder') {
        this.area.insertBefore(wrap, _this.adder);
    } else {
        this.area.appendChild(wrap);
    }
    
    if(this.ajax) {
        this.autocomplete(inputVisible);
    }
}

LabelManager.prototype.append = function(label) {
    this.appendField(label, 'default');
}

LabelManager.prototype.appendAdder = function(label) {
    this.appendField(label, 'adder');
}

LabelManager.prototype.onAdd = function(inputVisible, inputValue) {
	if(inputValue.value.length > 0) {
        	this.append({text: inputVisible.value, value: inputValue.value});
            inputValue.value = '';
            if(inputValue.hasAttribute('name')) {
                var inputs = inputValue.parentNode.getElementsByTagName('input');
                this.toggleInputsNames(inputs, true);
            }
        	inputVisible.value = '';
	}
}

LabelManager.prototype.parse = function() {
    var area = this.area.children;
    var toAppend = [];

    for(var i = 0; i < area.length; ++i) {
        var field = area[i];
        var children = field.children;
        if(hasClass(field, this.classNames.item)) {
            if(field.dataset.hasOwnProperty('name') && field.dataset.hasOwnProperty('id')) {
                toAppend.push({text: field.dataset.name, value: field.dataset.id});
                field.remove();
                --i;
            }
        }
    }

    this.appendAdder();

    for (i in toAppend) {
        this.append(toAppend[i]);
    }

}

LabelManager.prototype.onInputChange = function(input) {
}

LabelManager.prototype.autocomplete = function(input) {
    var _this = this;

    $(input).autocomplete({
        source: function(request, response) {
            if(request.term.length < 3) {
                return;
            }

            var xhr = new XMLHttpRequest();
            xhr.open('get', _this.xhrUrl.replace('%i', request.term));
            xhr.addEventListener('readystatechange', function() {
                if(xhr.status === 200 && xhr.readyState === 4) {
                    var res = JSON.parse(xhr.response);
                    _this.xhrMap(res, response);
                }
            });
            xhr.send();
        },

        select: function( event, ui ) {
            input.nextElementSibling.value = ui.item.value;
            if(!input.hasAttribute('name')) {
                var inputs = input.parentNode.getElementsByTagName('input');
                _this.toggleInputsNames(inputs);
            }
            input.value = ui.item.label;
            event.preventDefault();
        }
    });
}

LabelManager.prototype.toggleInputsNames = function(inputs, has = false) {
    for (var i = 0; i < inputs.length; ++i) {
        if(inputs[i].hasAttribute('name') == has) {
            toggleAttributes(inputs[i], 'data-name', 'name');
        }
    }
}

LabelManager.prototype.remove = function(wrap) {
    wrap.remove();
}