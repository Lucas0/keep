/**
 * Search class for projects
 *
 * @class      Search
 * @param      {class}  searchParser    The search parser
 * @param      {class}  searchRenderer  The search renderer
 */
function Search(searchParser, searchRenderer) {
    if(searchParser === undefined) {
        console.error("searchParser is undefined");
        return;
    }

    if(searchRenderer === undefined) {
        console.error("searchRenderer is undefined");
        return;
    }
    this.searchParser = searchParser;
    this.searchRenderer = searchRenderer;
    this.url = '/projets/s/';
    this.timer = 0;

    var fields = this.searchParser.getFields();
    var _this = this;
    for(k in fields) {
        if(fields[k].type.toLowerCase() == "range") {
            fields[k].slider.on('slide', function() {_this.triggerUpdate()});
        } else if(fields[k].field.type == 'text') {
            fields[k].field.addEventListener('input', function() {_this.triggerUpdate()});
        } else if(fields[k].type == 'checkbox') {
            for(var i = 0; i < fields[k].field.length; ++i) {
                fields[k].field[i].addEventListener('change', function() {_this.triggerUpdate()});
            }
        } else {
            fields[k].field.addEventListener('change', function() {_this.triggerUpdate()});
        }
    }
};

/**
 * Intermediary which waits some milliseconds for user input before send a request to the server
 */
Search.prototype.triggerUpdate = function() {
    clearTimeout(this.timer);

    var _this = this;
    this.timer = setTimeout(function() {_this.onUpdate()}, 425);
}

/**
 * Retrieves parameters and sends a request to the server
 */
Search.prototype.onUpdate = function() {
    var request = this.searchParser.getData();
    var _this = this;
    this.request(request, function(arg) {_this.searchRenderer.insertItems(arg)});
}

/**
 * Converts array parameters to url escaped string
 *
 * @param      {mixed array}  request  The parameters array
 * @return     {string}  stringified array
 */
Search.prototype.stringify = function(request) {
    var args = [];
    for(k in request) {
        if(k == 'keywords' || request[k] == '0') {
            continue;
        }

        args.push(encodeURIComponent(k) + "=" + encodeURIComponent(request[k]));
    }

    return "?" + args.join('&');
}

/**
 * Sends a search request to the server and call callback
 *
 * @param      {mixed array}    request   The parameters array
 * @param      {Function}  callback  The callback
 */
Search.prototype.request = function(request, callback) {
    var args = this.stringify(request);
    var xhr = new XMLHttpRequest();
    var _this = this;
    xhr.open('get', this.url + escape(request.keywords) + (args.length > 1 ? args : ''));
    xhr.addEventListener('readystatechange', function() {
        if(xhr.readyState === 4 && xhr.status == 200) {
            _this.searchRenderer.erase();
            var projects = JSON.parse(xhr.response);
            callback(projects);
        }
    });
    xhr.send();
}