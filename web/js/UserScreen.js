window.addEventListener('hashchange', iframeChange);

function iframeChange() {
    var hash = window.location.hash.substr(1).split(':');
    var project = '';
    var module = '';    
    

    if (hash.length >= 1 && hash[0] !== '')
    {
        project = hash[0];
    }

    if (hash.length === 2 && hash[1] !== '')
    {
        module = hash[1];
    }

    if(project !== '' || module !== '')
    {
        if(module === '') {
            $('.user-screen').attr('src', "//" + project + "." + window.location.host);
        }
        else 
        {
            $('.user-screen').attr('src', "//" + module + "-" + project + "." + window.location.host);
        }
    }
}

window.addEventListener('load', function() {
    iframeChange();
});