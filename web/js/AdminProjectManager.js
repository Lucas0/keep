/**
 * Class which handles the project management
 *
 * @class      AdminProjectManager
 * @param      {object}  fields  HTML elements used on the admin page
 */
function AdminProjectManager(fields) {
	this.root = document.getElementById(fields.root.slice(1));

    var _this = this;

    this.selectBtn = document.getElementById(fields.select.slice(1));
    this.selectBtn.addEventListener('click', function() {_this.onSelect();});

    this.editBtn = document.getElementById(fields.edit.slice(1));
    this.editBtn.addEventListener('click', function() {_this.onEdit();});

    this.removeBtn = document.getElementById(fields.remove.slice(1));
    this.removeBtn.addEventListener('click', function() {_this.onRemove();});
    
    this.selectArea = document.getElementById(fields.selectArea.slice(1));

	this.projects = {};
	this.selectedProjects = [];
	this.selectionMode = false;
};

/**
 * Watch a projet for events on the admin page
 *
 * @param      {object}  project  A project to watch
 */
AdminProjectManager.prototype.watch = function(project) {
	var adminProject = new AdminProject(project);
	this.projects[project.id] = adminProject;

	var _this = this;

	adminProject.addEventListener('selected', function() {
		_this.updateSelectedProjects(this);
	});

	project.description.addEventListener('click', function() {
		adminProject.toggleDescription();
		if(_this.selectionMode === false) {
			_this.reset();
			var checkbox = this.previousElementSibling;
			checkbox.checked = true;
			checkbox.dispatchEvent(new Event('change'));
			checkbox.checked = false;
		}
	});
}

/**
 * Resets the project view
 */
AdminProjectManager.prototype.reset = function() {
	this.selectedProjects = [];
	this.selectArea.innerHTML = '';
	this.updateButtonActivation();
}

/**
 * Function called when selection button is clicked
 * Show/Hide the checkboxs
 */
AdminProjectManager.prototype.onSelect = function() {
	this.reset();
	this.selectionMode = !this.selectionMode;

	prependedToggleTextBtn(this.selectBtn);
	prependedToggleTextBtn(this.removeBtn);

	for(k in this.projects) {
		this.projects[k].toggleCheckbox();

		if(this.selectionMode === false) {
			this.projects[k].fields.checkbox.checked = false;
		}
	}
}

/**
 * Function called when edit button is clicked
 * Redirects the user to the editing page when a project is selected
 */
AdminProjectManager.prototype.onEdit = function() {
	if(this.selectedProjects.length == 1) {
		window.location = '/projets/editer/' + this.selectedProjects[0].id;
	}
}

/**
 * Function called when remove button is clicked
 * Asks confirmation and remove the selected project(s)
 */
AdminProjectManager.prototype.onRemove = function() {
	if(this.selectedProjects.length > 0) {
		var ids = [];
		for(k in this.selectedProjects) {
			if(this.selectedProjects[k].hasct == true) {
				ids.push(this.selectedProjects[k].id);
			}
		}

		if(ids.length > 0) {
			if(!confirm('Êtes-vous sûr de vouloir procéder à la suppression ?')) {
				e.preventDefault();
			}
			this.deleteRequest(ids);
		}
	}
	
	e.preventDefault();
}

/**
 * Add or remove a projet from the selection list
 *
 * @param      {object}  project  Project to add or remove
 */
AdminProjectManager.prototype.updateSelectedProjects = function(project) 
{
	if(project.checkbox.checked == true || this.selectionMode === false)
	{
		var li = document.createElement('li');
		this.selectedProjects.push({
			'id': project.id,
			'name': project.name,
			'hasct': project.hasct
		});

		li.innerHTML = project.name;
		li.dataset.id = project.id;
		li.dataset.hasct = project.hasct;
		this.selectArea.appendChild(li);
	} else {
		this.deselectProjectById(project.id);
	}

	this.updateButtonActivation();
}

/**
 * Enables or disables remove and edit buttons regarding of user interaction
 */
AdminProjectManager.prototype.updateButtonActivation = function()
{
	if(this.areActiveSelectedProject()) {
		this.removeBtn.removeAttribute("disabled");
	} else {
		this.removeBtn.setAttribute('disabled', "disabled");
	}

	if(this.selectedProjects.length == 1) {
		this.editBtn.removeAttribute("disabled");
	} else {
		this.editBtn.setAttribute('disabled', "disabled");
	}
}

/**
 * Gets a selected project by its identifier
 *
 * @param      {number}  id      The identifier of a project
 * @return     {number}   The selected project or -1 if not found
 */
AdminProjectManager.prototype.getSelectedById = function(id) {
	for (var i = 0; i < this.selectedProjects.length; i++) {
		if(this.selectedProjects[i].id == id) {
			return i;
		}
	}

	return -1;
}

/**
 * Remove a project from the selection list
 *
 * @param      {number}  id      The project identifier
 */
AdminProjectManager.prototype.deselectProjectById = function(id) {
	var index = this.getSelectedById(id);

	if(index >= 0) {
		var lis = this.selectArea.getElementsByTagName('li');
		for (var i = 0; i < lis.length; ++i) {
			if(lis[i].dataset.id == this.selectedProjects[index].id)
			{
				this.selectArea.removeChild(lis[i]);
			} 
		}
		this.selectedProjects.splice(index, 1);
	}
}

/**
 * Returns if there are active project in the selected projects
 *
 * @return     {boolean}  True if are active projects, False otherwise
 */
AdminProjectManager.prototype.areActiveSelectedProject = function() {
	for (var i = 0; i < this.selectedProjects.length; i++) {
		if(this.selectedProjects[i].hasct == true) {
			return true;
		}
	}

	return false;
}

/**
 * Determines if selection mode is enabled
 *
 * @return     {boolean}  True if selection mode enabled, False otherwise
 */
AdminProjectManager.prototype.isSelectionModeEnabled = function() {
	return this.selectionMode;
}

/**
 * Do a delete request of one or several projects
 *
 * @param      {number[]}  ids     The identifiers of the projects
 */
AdminProjectManager.prototype.deleteRequest = function(ids) {
	for(i in ids) {
		var xhr = new XMLHttpRequest();
		xhr.open('DELETE', "/projets/" + ids[i]);
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xhr.addEventListener('readystatechange', function() {
			if(xhr.readyState == 4) {
				if(xhr.status != 200) {
					alert("Un problème est survenu lors de la suppression du conteneur.");
				}
			}
		});
		
		xhr.send("_method=DELETE&form[_token]=" + this.removeBtn.dataset.token);
	}
}