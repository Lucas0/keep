/**
 * Search renderer for the user interface
 *
 * @class      SearchHomeRenderer
 * @param      {array}  fields               The fields' html class and identifiers
 */
function SearchHomeRenderer(fields) {
    SearchRenderer.apply(this, fields);
    if(fields === undefined) {
        console.error("fields is undefined");
        return;
    }

    this.root = document.getElementById(fields.root.slice(1));
    if(this.root === null) {
        console.error("root field not found");
        return;
    }
};

/**
 * Erases the search results
 */
SearchHomeRenderer.prototype.erase = function() {
    this.root.innerHTML = '';
}

/**
 * Inserts an item into the list
 *
 * @param      {array}  item    The item to append
 */
SearchHomeRenderer.prototype.insertItem = function(item) {
    var mainDiv = createNodeWithParams('div', {'class': 'row callout'});

    var imgDiv = createNodeWithParams('div', {'class': 'large-2 column'});
    mainDiv.appendChild(imgDiv);

    var img = createNodeWithParams('img', {'src': 'http://placehold.it/140x100'});
    imgDiv.appendChild(img);

    var contentDiv = createNodeWithParams('div', {'class': 'large-10 column'});
    mainDiv.appendChild(contentDiv);

    var a = createNodeWithParams('a', {'href': '#'});

    if(item.published) {
        a.href = "//" + item.slug + "." + window.location.host;
    }

    contentDiv.appendChild(a);

    var title = createNodeWithParams('p', {'class': 'lead pst-title'});
    title.innerText = item.name;
    a.appendChild(title);

    var categories = createNodeWithParams('h6', {'class': 'subheader'});
    for(cat in item.categories) {
        var category = createNodeWithParams('span', {'class': 'category', 'innerHTML': item.categories[cat].name});
        categories.appendChild(category);
        categories.innerHTML += ' ';
    }   
    a.appendChild(categories);

    var description = createNodeWithParams('p', {'class': 'subheader'});
    description.innerText = item.description;
    a.appendChild(description);

    var info = createNodeWithParams('h6', {'class': 'subheader'});

    var students = [];
    var tutors = [];
    var grades = [];

    if(item.members !== null) {
        for(m in item.members) {
            if(item.members[m].role.name == "student") {
                students.push(item.members[m].user.lastname + ' ' + item.members[m].user.firstname);
                if(grades.indexOf(item.members[m].grade) === -1) {
                    grades.push(item.members[m].grade);
                }
            } else if(item.members[m].role.name == "tutor") {
                tutors.push(item.members[m].user.lastname + ' ' + item.members[m].user.firstname);
            }
        }

        info.innerHTML = 'Par ' + students.join(', ');
        info.innerHTML += ' suivi par ' + tutors.join(', ');
    }

    if(item.campus !== null) {
        info.innerHTML += ' à ';
        for(i in item.campus) {
            info.innerHTML += item.campus[i].school.name + ' ' + item.campus[i].city.name;
            if(item.campus[i].name !== null) {
                info.innerHTML += ' ' + item.campus[i].name;
            }
        }
    }

    if(item.year !== null) {
        info.innerHTML += ' en ' + (item.year - 1) + '-' + item.year;
    }

    if(item.grades !== null) {
        info.innerHTML += ' (' + grades.join(', ') + '<sup>e</sup> année).';
    }

    a.appendChild(info);

    this.root.appendChild(mainDiv);
}

/**
 * Calls insertItem for an array of item
 *
 * @param      {array}  items   The items
 */
SearchHomeRenderer.prototype.insertItems = function(items) {
    for(k in items) {
        this.insertItem(items[k]);
    }
}

Object.setPrototypeOf(SearchHomeRenderer, SearchRenderer);
