/**
 * Abstract class for search rendering
 *
 * @class      SearchRenderer
 * @param      {array}  fields  The fields' html class and identifiers
 */
function SearchRenderer(fields) {
    if(this.constructor === SearchRenderer) {
        throw new Error("Can't instantiate abstract class!");
    }
};

/**
 * Erases the search results
 */
SearchRenderer.prototype.erase = function() {
}

/**
 * Inserts an item into the list
 *
 * @param      {array}  item    The item to append
 */
SearchRenderer.prototype.insertItem = function(item) {
}

/**
 * Calls insertItem for an array of item
 *
 * @param      {array}  items   The items
 */
SearchRenderer.prototype.insertItems = function(items) {
    for(k in items) {
        this.insertItem(items[k]);
    }
}