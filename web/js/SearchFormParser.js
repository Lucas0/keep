/**
 * Class which retrieves search data from web page
 *
 * @class      SearchFormParser
 * @param      {array}  fields  The fields' html class and identifiers to retrieve
 */
function SearchFormParser(fields) {
    this.fields = {};
    for(f in fields) {
        var fieldName = f.slice(1);
        if(f[0] === '#') {
            var field = document.getElementById(fieldName);
            if(field === null) {
                console.error("Unknown " + fieldName + " field.");
                continue;
            }
            this.fields[fieldName] = fields[f];
            this.fields[fieldName].field = field;

            if(this.fields[fieldName].alias === undefined) {
                this.fields[fieldName].alias = fieldName;
            }

            if(this.fields[fieldName].type === undefined) {
                this.fields[fieldName].type = "text";
            }
        } else if(f[0] === '.') {
            var field = document.getElementsByClassName(fieldName);
            if(field === null) {
                console.error("Unknown " + fieldName + " field.");
                continue;
            }

            if(fields[f].type !== undefined && fields[f].type.toLowerCase() === 'checkbox')
            this.fields[fieldName] = fields[f];
            this.fields[fieldName].field = field;

            if(this.fields[fieldName].alias === undefined) {
                this.fields[fieldName].alias = fieldName;
            }
        }
    }
};

/**
 * Gets the fields
 *
 * @return     {array}  The fields
 */
SearchFormParser.prototype.getFields = function() {
    return this.fields;
}

/**
 * Gets the data
 *
 * @return     {array}  The data.
 */
SearchFormParser.prototype.getData = function() {
    var data = {};

    for(f in this.fields) {
        if(this.fields[f].type === "text") {
            data[this.fields[f].alias] = this.fields[f].field.value;
        } else if(this.fields[f].type === "range") {
            data[this.fields[f].alias] = this.fields[f].field.value.split(',');
        } else if(this.fields[f].type === "checkbox") {
            data[this.fields[f].alias] = getSelectedCheckbox(this.fields[f].field);
        }
    }

    return data;
}
